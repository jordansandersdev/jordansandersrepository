﻿CREATE TABLE [HumanResources].[Example] (
  [EmployeeID] [int] NOT NULL,
  [JobTitle] [nvarchar](50) NULL,
  [DepartmentName] [nvarchar](50) NULL,
  [Salary] [money] NULL,
  PRIMARY KEY CLUSTERED ([EmployeeID])
)
ON [PRIMARY]
GO