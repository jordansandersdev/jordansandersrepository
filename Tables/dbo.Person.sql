﻿CREATE TABLE [dbo].[Person] (
  [PersonId] [int] IDENTITY,
  [Name] [varchar](100) NULL,
  [Password] [varchar](50) COLLATE Latin1_General_BIN2 NOT NULL,
  [SSN] [varchar](11) COLLATE Latin1_General_BIN2 NOT NULL,
  PRIMARY KEY CLUSTERED ([PersonId])
)
ON [PRIMARY]
GO