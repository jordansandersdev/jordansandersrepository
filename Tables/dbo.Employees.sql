﻿CREATE TABLE [dbo].[Employees] (
  [ID] [int] NOT NULL,
  [NAME] [varchar](20) NOT NULL,
  [AGE] [int] NOT NULL,
  [ADDRESS] [char](25) NULL,
  [SALARY] [decimal](18, 2) NULL,
  PRIMARY KEY CLUSTERED ([ID])
)
ON [PRIMARY]
GO