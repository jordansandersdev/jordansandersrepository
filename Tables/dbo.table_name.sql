﻿CREATE TABLE [dbo].[table_name] (
  [ID] [int] NOT NULL,
  CONSTRAINT [PK_table_name_ID] PRIMARY KEY CLUSTERED ([ID])
)
ON [PRIMARY]
GO