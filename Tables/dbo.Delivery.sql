﻿CREATE TABLE [dbo].[Delivery] (
  [ID] [int] NOT NULL,
  [Product_name] [nvarchar](30) NULL,
  [Terms] [nvarchar](15) NULL
)
ON [PRIMARY]
GO