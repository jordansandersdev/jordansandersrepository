﻿CREATE TABLE [dbo].[Products] (
  [ID] [int] NOT NULL,
  [Product_name] [nvarchar](30) NULL,
  [Delivery] [nvarchar](15) NULL
)
ON [PRIMARY]
GO