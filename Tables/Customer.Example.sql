﻿CREATE TABLE [Customer].[Example] (
  [CustomerID] [int] NULL,
  [AccountNumber] [varchar](10) NULL,
  [CustomerType] [varchar](1) NULL,
  [AddressLine1] [varchar](50) NULL,
  [City] [varchar](50) NULL,
  [AddressType] [varchar](50) NULL
)
ON [PRIMARY]
GO