﻿CREATE TABLE [dbo].[Address] (
  [ID] [int] NOT NULL,
  [City] [nvarchar](30) NULL,
  [PostalCode] [nvarchar](15) NULL
)
ON [PRIMARY]
GO