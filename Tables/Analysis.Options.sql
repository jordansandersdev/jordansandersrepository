﻿CREATE TABLE [Analysis].[Options] (
  [AnalysisID] [int] NULL,
  [width] [decimal](10, 2) NULL
)
ON [PRIMARY]
GO