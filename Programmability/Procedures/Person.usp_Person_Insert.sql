﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE PROC [Person].[usp_Person_Insert]
    @BusinessEntityID int,
    @PersonType nchar(2),
    @NameStyle dbo.NameStyle,
    @Title nvarchar(8),
    @FirstName dbo.Name,
    @MiddleName dbo.Name,
    @LastName dbo.Name,
    @Suffix nvarchar(10),
    @EmailPromotion int,
    @AdditionalContactInfo xml (CONTENT Person.AdditionalContactInfoSchemaCollection),
    @Demographics xml (CONTENT Person.IndividualSurveySchemaCollection),
    @rowguid uniqueidentifier,
    @ModifiedDate datetime,
    @PersonID int
AS 
    SET NOCOUNT ON
    SET XACT_ABORT ON

    BEGIN TRAN

    INSERT INTO Person.Person (BusinessEntityID, PersonType, NameStyle, Title, FirstName, MiddleName, 
                               LastName, Suffix, EmailPromotion, AdditionalContactInfo, Demographics, 
                               rowguid, ModifiedDate, PersonID)
    SELECT @BusinessEntityID, @PersonType, @NameStyle, @Title, @FirstName, @MiddleName, @LastName, @Suffix, 
           @EmailPromotion, @AdditionalContactInfo, @Demographics, @rowguid, @ModifiedDate, @PersonID

    /*
    -- Begin Return row code block

    SELECT BusinessEntityID, PersonType, NameStyle, Title, FirstName, MiddleName, LastName, Suffix, 
           EmailPromotion, AdditionalContactInfo, Demographics, rowguid, ModifiedDate, PersonID
    FROM   Person.Person
    WHERE  BusinessEntityID = @BusinessEntityID AND PersonType = @PersonType AND NameStyle = @NameStyle AND 
           Title = @Title AND FirstName = @FirstName AND MiddleName = @MiddleName AND LastName = @LastName AND 
           Suffix = @Suffix AND EmailPromotion = @EmailPromotion AND AdditionalContactInfo = @AdditionalContactInfo AND 
           Demographics = @Demographics AND rowguid = @rowguid AND ModifiedDate = @ModifiedDate AND 
           PersonID = @PersonID

    -- End Return row code block

    */
    COMMIT
GO