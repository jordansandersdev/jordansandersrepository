﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE PROC [Person].[usp_Person_Update]
@BusinessEntityID int,
@PersonType nchar(2),
@NameStyle dbo.NameStyle,
@Title nvarchar(8),
@FirstName dbo.Name,
@MiddleName dbo.Name,
@LastName dbo.Name,
@Suffix nvarchar(10),
@EmailPromotion int,
@AdditionalContactInfo xml (CONTENT Person.AdditionalContactInfoSchemaCollection),
@Demographics xml (CONTENT Person.IndividualSurveySchemaCollection),
@rowguid uniqueidentifier,
@ModifiedDate datetime,
@PersonID int
AS 
    SET NOCOUNT ON
    SET XACT_ABORT ON

    BEGIN TRAN

    UPDATE Person.Person
    SET    PersonType = @PersonType, NameStyle = @NameStyle, Title = @Title, FirstName = @FirstName, 
           MiddleName = @MiddleName, LastName = @LastName, Suffix = @Suffix, EmailPromotion = @EmailPromotion, 
           AdditionalContactInfo = @AdditionalContactInfo, Demographics = @Demographics, ModifiedDate = @ModifiedDate, 
           PersonID = @PersonID
    WHERE  BusinessEntityID = @BusinessEntityID AND rowguid = @rowguid

    /*
    -- Begin Return row code block

    SELECT PersonType, NameStyle, Title, FirstName, MiddleName, LastName, Suffix, EmailPromotion, AdditionalContactInfo, 
           Demographics, ModifiedDate, PersonID
    FROM   Person.Person
    WHERE  BusinessEntityID = @BusinessEntityID AND rowguid = @rowguid

    -- End Return row code block

    */
    COMMIT
GO