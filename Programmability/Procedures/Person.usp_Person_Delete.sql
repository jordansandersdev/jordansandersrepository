﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE PROC [Person].[usp_Person_Delete]
@BusinessEntityID1 int,
@rowguid1 uniqueidentifier
AS 
    SET NOCOUNT ON
    SET XACT_ABORT ON

    BEGIN TRAN

    DELETE
    FROM   Person.Person
    WHERE  BusinessEntityID = @BusinessEntityID1 AND rowguid = @rowguid1

    COMMIT
GO