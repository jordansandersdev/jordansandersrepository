﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE PROC [Person].[usp_Person_Select]
    @BusinessEntityID int,
    @rowguid uniqueidentifier
AS
    SET NOCOUNT ON
    SET XACT_ABORT ON

    BEGIN TRAN

    SELECT BusinessEntityID, PersonType, NameStyle, Title, FirstName, MiddleName, LastName, Suffix, EmailPromotion, AdditionalContactInfo, Demographics, rowguid, ModifiedDate, PersonID
    FROM   Person.Person
    WHERE  BusinessEntityID = @BusinessEntityID AND rowguid = @rowguid 

    COMMIT
GO