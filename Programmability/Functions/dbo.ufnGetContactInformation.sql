﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[ufnGetContactInformation] ()
RETURNS @contactInfo TABLE (
  [PersonID] INT NOT NULL
 ,[FirstName] [NVARCHAR](50) NULL
)
AS
BEGIN
  INSERT INTO @contactInfo
    SELECT
      p.BusinessEntityID
     ,p.FirstName
    FROM Person.Person p
  RETURN;
END;
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Table value function returning the first name, last name, job title and contact type for a given contact.', 'SCHEMA', N'dbo', 'FUNCTION', N'ufnGetContactInformation'
GO