﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

  CREATE VIEW [dbo].[EmployeeRecords]
AS
     SELECT *
     FROM [HumanResources].[Employee];
GO